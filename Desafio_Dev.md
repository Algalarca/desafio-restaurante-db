# Introdução
Os times da DBserver enfrentam um grande problema. Como eles são muito democráticos, todos os dias eles gastam 30 minutos decidindo onde eles irão almoçar.
Vamos fazer um pequeno sistema que auxilie essa tomada de decisão!

## Instruções:
##### Crie um readme que inclua:
* Requisitos de ambiente necessários para compilar e rodar o software
* Instruções de como utilizar o sistema.
* O que vale destacar no código implementado?
* O que poderia ser feito para melhorar o sistema?
* Algo a mais que você tenha a dizer 

## Estórias

________________________________________
**Estória 1**
Eu como **profissional faminto**
Quero votar no meu restaurante favorito
Para que eu consiga democraticamente levar meus colegas a comer onde eu gosto.
**Critério de Aceitação**
* Um profissional só pode votar em um restaurante por dia.

________________________________________
**Estória 2**
Eu como **facilitador do processo de votação**
Quero que um restaurante não possa ser repetido durante a semana
Para que não precise ouvir reclamações infinitas!
**Critério de Aceitação**
* O mesmo restaurante não pode ser escolhido mais de uma vez durante a semana.

________________________________________
**Estória 3**
Eu como **profissional faminto**
Quero saber antes do meio dia qual foi o restaurante escolhido
Para que eu possa me despir de preconceitos e preparar o psicológico.
**Critério de Aceitação**
* Mostrar de alguma forma o resultado da votação.