# Desafio do Restaurante DBServer

### Introdução
```
Os times da DBserver enfrentam um grande problema. Como eles são muito democráticos, todos os dias eles gastam 30 minutos decidindo onde eles irão almoçar.
Faça um pequeno sistema que auxilie essa tomada de decisão!
```
### Crie um readme que inclua:
- Requisitos de ambiente necessários para compilar e rodar o software
- Instruções de como utilizar o sistema.
- O que vale destacar no código implementado?
- O que poderia ser feito para melhorar o sistema?
- Algo a mais que você tenha a dizer 

# INSTRUCTIONS
* 1. To run the system, open the Package Manager Console in Visual Studio IDE and run the command 'Update-Database', in order to build the database structure and create the database itself;
* 2. Run the project: CTRL + F5

### Notes:
* User must log in in order to vote in a a restaurant;
* User doesn't need to be logged in to view the list of most voted restaurants, and see the details of each restaurant;
* New users need to be registered prior to first use;
* The password for a new user has to comply with the following rules: at least one Uppercase char, lowercase chars, numbers, at least one wildcard (@,*,$ etc) and minimum length 8 chars.

### WIP features:
* An ADMIN area and an Admin User: One SuperUser will be in charge of add/edit/delete users, add/edit/delete restaurants, add another SuperUser;
* first SuperUser will be added at the start of the program, only once:
** Username: Admin
** Password: P0oi#.ty
-----------------------------------
(EXCLUIR APÓS PRONTO) - VS Task List

- RESTAURANT CHALLENGE

ADIÇÕES:

- Search field do nome do restaurante (igual ao search do nome, descrição dos lanches);
- criar usuário ADMIN no início do app - igual ao Lanches;
- login;
- área do ADMIN;
- se o usuário é ADMIN, os campos para EDIT, DELETE, CREATE NEW ficam visíveis;
- se o usuário não é ADMIN, fica visível somente o botão para votar no restaurante;
- mostrar na listagem inicial dos restaurantes, os restaurantes por ORDEM ALFABÉTICA ou QUANTIDADE DE VOTOS(maior primeiro) ???