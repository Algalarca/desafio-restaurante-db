﻿//using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RestaurantChallengeDBServer.Interfaces;

namespace RestaurantChallengeDBServer.Controllers
{
    public class RestaurantController : Controller
    {
        private readonly IRestaurantRepository _restaurantRepository;

        public RestaurantController(IRestaurantRepository restaurantRepository)
        {
            _restaurantRepository = restaurantRepository;
        }

        public IActionResult List()
        {
            //ViewBag.Restaurant = "Restaurants";
            //ViewData["Restaurant"] = "Restaurants";

            var restaurants = _restaurantRepository.Restaurants;

            return View(restaurants);
        }

        //[Authorize]
        public IActionResult Vote()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("List", "Restaurant");
                //para fins de referência, APAGAR DEPOIS
                //return View();
            }

            return RedirectToAction("Login", "Account");
        }
    }
}