﻿using System.ComponentModel.DataAnnotations;

namespace RestaurantChallengeDBServer.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}
