﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantChallengeDBServer.Migrations
{
    public partial class PopulateRestaurantsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Restaurants(Name, QtyVotes) VALUES('Restaurante Panorâmico', 0)");
            migrationBuilder.Sql("INSERT INTO Restaurants(Name, QtyVotes) VALUES('Restaurante Prédio 32', 0)");
            migrationBuilder.Sql("INSERT INTO Restaurants(Name, QtyVotes) VALUES('Restaurante Prédio 50', 0)");
            migrationBuilder.Sql("INSERT INTO Restaurants(Name, QtyVotes) VALUES('Restaurante Prédio 5', 0)");
            migrationBuilder.Sql("INSERT INTO Restaurants(Name, QtyVotes) VALUES('Restaurante Palatus', 0)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Restaurants");
        }
    }
}
