﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RestaurantChallengeDBServer.Models;

namespace RestaurantChallengeDBServer.DBContext
{
    public class RestaurantChallengeDBContext : IdentityDbContext<IdentityUser>
    {
        public RestaurantChallengeDBContext(DbContextOptions<RestaurantChallengeDBContext> options)
            : base(options)
        { }

        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<PollData> PollData { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}
