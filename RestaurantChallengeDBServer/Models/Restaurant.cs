﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantChallengeDBServer.Models
{
    public class Restaurant
    {
        public int RestaurantId { get; set; }
        [Required(ErrorMessage = "Informe o nome do Restaurante:")]
        [StringLength(80)]
        [Display(Name = "Nome")]
        public string Name { get; set; }
        [Display(Name = "Quantidade de Votos")]
        public int QtyVotes { get; set; }

        //TABLE RELATIONSHIP - one to many
        //public ICollection<User> UserVotes { get; set; } = new List<User>();
    }
}