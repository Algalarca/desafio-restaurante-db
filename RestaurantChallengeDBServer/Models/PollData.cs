﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantChallengeDBServer.Models
{
    public class PollData
    {
        public int PollDataId { get; set; }
        public int EmployeeId { get; set; }
        public int RestaurantId { get; set; }
        public DateTime VoteDay { get; set; }
        public int WeekOfYear { get; set; }
        public int Weekday { get; set; }
        public bool Winner { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Restaurant Restaurant { get; set; }

        //para referência, apagar depois
        //public virtual Categoria Categoria { get; set; }
    }
}
