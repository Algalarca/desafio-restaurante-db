﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantChallengeDBServer.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        //public virtual int MyProperty { get; set; }
    }
}



//public int RestaurantId { get; set; }
//[Required(ErrorMessage = "Informe o nome do Restaurante:")]
//[StringLength(80)]
//[Display(Name = "Nome")]
//public string Name { get; set; }
//[Display(Name = "Quantidade de Votos")]
//public int QtyVotes { get; set; }
//TABLE RELATIONSHIP - one to many
//public ICollection<User> UserVotes { get; set; } = new List<User>();