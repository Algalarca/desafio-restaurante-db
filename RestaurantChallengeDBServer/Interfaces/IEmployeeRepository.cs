﻿using RestaurantChallengeDBServer.Models;
using System.Collections.Generic;

namespace RestaurantChallengeDBServer.Interfaces
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> Employees { get; }
    }
}
