﻿using RestaurantChallengeDBServer.Models;
using System.Collections.Generic;

//padrão Repositório
namespace RestaurantChallengeDBServer.Interfaces
{
    public interface IRestaurantRepository
    {
        IEnumerable<Restaurant> Restaurants { get; }
    }
}
