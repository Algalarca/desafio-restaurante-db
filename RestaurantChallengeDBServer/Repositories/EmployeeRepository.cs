﻿using RestaurantChallengeDBServer.DBContext;
using RestaurantChallengeDBServer.Interfaces;
using RestaurantChallengeDBServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantChallengeDBServer.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly RestaurantChallengeDBContext context;

        public EmployeeRepository (RestaurantChallengeDBContext context)
        {
            this.context = context;
        }

        public IEnumerable<Employee> Employees => context.Employees;
    }
}
