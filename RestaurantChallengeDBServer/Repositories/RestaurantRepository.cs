﻿using RestaurantChallengeDBServer.DBContext;
using RestaurantChallengeDBServer.Interfaces;
using RestaurantChallengeDBServer.Models;
using System.Collections.Generic;

//padrão Repositório
namespace RestaurantChallengeDBServer.Repositories
{
    public class RestaurantRepository : IRestaurantRepository
    {
        private readonly RestaurantChallengeDBContext _context;

        public RestaurantRepository(RestaurantChallengeDBContext context)
        {
            _context = context;
        }

        public IEnumerable<Restaurant> Restaurants => _context.Restaurants;
    }
}
