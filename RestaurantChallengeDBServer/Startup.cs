﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RestaurantChallengeDBServer.DBContext;
using RestaurantChallengeDBServer.Interfaces;
using RestaurantChallengeDBServer.Repositories;

namespace RestaurantChallengeDBServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<RestaurantChallengeDBContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("RestaurantChallengeDbConnectionString")));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<RestaurantChallengeDBContext>()
                .AddDefaultTokenProviders();

            //padrão Repositório
            //instância da implementação da interface
            services.AddTransient<IRestaurantRepository, RestaurantRepository>();
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                        name: "restaurantList",
                        template: "{controller=Restaurant}/{action=List}/{id?}");
                //defaults: new { controller = "Restaurant", action = "List" });

                //retirar controller HOME???
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}


//PRIORITY
/*
 * - um voto por dia, de cada usuário, até o meio-dia;
 * - o restaurante ganhador do dia não pode ser mais selecionado na semana (não deveria mais aparecer na listagem???);
 * - até o meio-dia, mostrar o restaurante ganhador do dia.
 * - mostrar na listagem inicial dos restaurantes, os restaurantes por ORDEM ALFABÉTICA ou QUANTIDADE DE VOTOS(maior primeiro) ???
*/

//TODO
/*
 * a senha para cadastrar necessita ter letras maiúscula, letras minúscula, números e algum digito como @,*,$ e etc. (informar na tela de cadastro de usuário)
 * Adicionar 'PlaceHolder' com informações do que é necessário para a senha do usuário
 * 38. Rotas - Definindo uma nova rota na aplicação - 10m *****
 * 24. ViewModel - Conceito - 3m ******
 * 25. Implementando o padrão ViewModel - 6m *****
 * Seção 4 (21 22 26 33 34 35)
 * Seção 5-(44) Search field do nome do restaurante(igual ao search do nome, descrição dos lanches); - 'PLACEHOLDER PARA O 'SEARCH' DE RESTAURANTES' (ver no Startup.cs)
 * - criar usuário ADMIN no início do app - igual ao Lanches;
 * - login;
 * - área do ADMIN;
 * - se o usuário é ADMIN, os campos para EDIT, DELETE, CREATE NEW ficam visíveis;
 * - se o usuário não é ADMIN, fica visível somente o botão para votar no restaurante, e o botão de 'Details';
*/
